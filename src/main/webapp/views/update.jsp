<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: rajiv
  Date: 7/17/2018
  Time: 5:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  Created by IntelliJ IDEA.
  User: rajiv
  Date: 7/13/2018
  Time: 10:45 PM
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <title>Customer</title>
</head>
<body class="container">


<h1>Update Customer</h1>
<hr/>
<form method="post" action="/home/update">


<%--<c:forEach items="${customers}" var="c">--%>

    <div class="form-group">
        <input type= "hidden" name="id" value="<c:out value="${customer.id}"/>">
    </div>
    <div class="form-group">

        <label for="firstname">First Name</label>

        <input type="text" class="form-control" id="firstname" name="firstname" value="<c:out value="${customer.getFirstName()}"/>" placeholder="First Name" >
    </div>

    <div class="form-group">
        <label for="lastname">Last Name</label>
        <input type="text" class="form-control" id="lastname" name="lastname"  value="<c:out value="${customer.getLastName()}"/>" placeholder="Last Name" >
    </div>

    <div class="form-group">
        <label for="age">Age</label>
        <input type="number" class="form-control" id="age" name="age" value="<c:out value="${customer.getAge()}"/>" placeholder="Age">
    </div>


    <input type="submit" class="btn btn-success" value="Save"/>

    <a href="<c:url value="/home" />" class="btn btn-danger">Back</a>
<%--</c:forEach>--%>
</form>

</body>
</html>
