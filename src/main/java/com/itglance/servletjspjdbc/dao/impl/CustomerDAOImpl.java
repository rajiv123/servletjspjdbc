package com.itglance.servletjspjdbc.dao.impl;

import com.itglance.servletjspjdbc.dao.CustomerDAO;
import com.itglance.servletjspjdbc.entity.Customer;
import com.itglance.servletjspjdbc.utils.DBConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAOImpl implements CustomerDAO {

    private DBConnection connection = new DBConnection();

    public List<Customer> getAll() throws ClassNotFoundException, SQLException {
        List<Customer> customerList = new ArrayList<Customer>();
        connection.open();

        String sql = "SELECT * FROM customers";
        connection.initStatement(sql);
        ResultSet rs = connection.executeQuery();

        while(rs.next()) {
            Customer c = new Customer();
            c.setId(rs.getInt("id"));
            c.setFirstName(rs.getString("first_name"));
            c.setLastName(rs.getString("last_name"));
            c.setAge(rs.getInt("age"));
            customerList.add(c);
        }

        connection.close();
        return customerList;
    }

    public int insert(Customer customer) throws ClassNotFoundException, SQLException {
        connection.open();
        String sql = "INSERT INTO customers(first_name, last_name, age) value(?, ?, ?);";
        PreparedStatement stmt = connection.initStatement(sql);
        stmt.setString(1, customer.getFirstName());
        stmt.setString(2, customer.getLastName());
        stmt.setInt(3, customer.getAge());
        int result = stmt.executeUpdate();
        connection.close();
        return result;
    }

    public int update(Customer customer) throws ClassNotFoundException, SQLException {
        connection.open();
        String sql="UPDATE customers set first_name=?,last_name=?,age=? where id=?";
        PreparedStatement stmt=connection.initStatement(sql);
        stmt.setString(1,customer.getFirstName());
        stmt.setString(2,customer.getLastName());
        stmt.setInt(3,customer.getAge());
        stmt.setInt(4,customer.getId());

        int result=stmt.executeUpdate();
        connection.close();
        return result;
    }

    public Customer getRecordById(int id) throws ClassNotFoundException, SQLException {
        Customer customer=null;
        connection.open();
        String sql="Select * from customers where id=?";
        PreparedStatement stmt=connection.initStatement(sql);
        stmt.setInt(1,id);
        ResultSet rs=connection.executeQuery();
        while(rs.next())
        {
            customer=new Customer();
            customer.setId(rs.getInt("Id"));
            customer.setFirstName(rs.getString("first_name"));
            customer.setLastName(rs.getString("last_name"));
            customer.setAge(rs.getInt("age"));
        }
        connection.close();
        return customer;
    }

    public int delete(int id) throws ClassNotFoundException, SQLException {
        connection.open();
        String sql="DELETE from customers where id=?";
        PreparedStatement stmt=connection.initStatement(sql);
        stmt.setInt(1, id);
        int result=stmt.executeUpdate();
        connection.close();
        return result;

    }
}
