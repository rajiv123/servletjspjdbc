package com.itglance.servletjspjdbc.dao;

import com.itglance.servletjspjdbc.entity.Customer;

import java.sql.SQLException;
import java.util.List;

public interface CustomerDAO {

    List<Customer> getAll() throws ClassNotFoundException, SQLException;

    int insert(Customer customer) throws ClassNotFoundException, SQLException;

    int update(Customer customer) throws ClassNotFoundException, SQLException;

    Customer getRecordById(int id) throws ClassNotFoundException, SQLException;

    int delete(int id) throws ClassNotFoundException, SQLException;


}
