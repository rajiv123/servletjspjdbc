package com.itglance.servletjspjdbc.servlet;

import com.itglance.servletjspjdbc.dao.CustomerDAO;
import com.itglance.servletjspjdbc.dao.impl.CustomerDAOImpl;
import com.itglance.servletjspjdbc.entity.Customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HomeServlet extends HttpServlet {

    CustomerDAO customerDAO = new CustomerDAOImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getRequestURI();
        if (url.contains("/add")) {
            request.getRequestDispatcher("/views/add.jsp").forward(request, response);
        } else if (url.contains("/delete")) {

            try {
                customerDAO.delete(Integer.parseInt(request.getParameter("id")));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            response.sendRedirect("/");
        } else if (url.contains("/update")) {

            String id = request.getParameter("id");

            Customer customer = null;
            try {

                customer = customerDAO.getRecordById(Integer.parseInt(id));

                request.setAttribute("customer", customer);

                request.getRequestDispatcher("/views/update.jsp").forward(request, response);

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                //error page
            } catch (SQLException e) {
                e.printStackTrace();
                // error page
            }
        } else {
            List<Customer> customerList = null;
            try {
                customerList = customerDAO.getAll();
            } catch (ClassNotFoundException e) {
                // error page
                e.printStackTrace();
            } catch (SQLException e) {
                // errror page
                e.printStackTrace();
            }
            request.setAttribute("customers", customerList);
            request.getRequestDispatcher("/views/index.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getRequestURI();
        if (url.contains("/save")) {
            try {
                Customer customer = new Customer();
                customer.setFirstName(request.getParameter("firstname"));
                customer.setLastName(request.getParameter("lastname"));
                customer.setAge(Integer.parseInt(request.getParameter("age")));
                customerDAO.insert(customer);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                // error page
            } catch (SQLException e) {
                // error page
                e.printStackTrace();
            } catch (ArithmeticException ae) {
                //error page
                ae.printStackTrace();
            }
            response.sendRedirect("/");
        } else if(url.contains("/update")) {
            try {
                Customer customer = new Customer();
                customer.setId(Integer.parseInt(request.getParameter("id")));
                customer.setFirstName(request.getParameter("firstname"));
                customer.setLastName(request.getParameter("lastname"));
                customer.setAge(Integer.parseInt(request.getParameter("age")));

                if(customerDAO.update(customer) > 0) {
                    response.sendRedirect("/");
                }else {
                    //  do something else
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                // error page
            } catch (SQLException e) {
                // error page
                e.printStackTrace();
            } catch (ArithmeticException ae) {
                //error page
                ae.printStackTrace();
            }
        }
    }
}
